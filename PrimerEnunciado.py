from Diagnostico import Imprimidor
import sys

def primer_enunciado (rango):
    
        num1 = int(rango[0])
        num2 = int(rango[1])

        i = Imprimidor(num1,num2)
        fizz = i.agregar_condicion(3,"fizz")
        buzz = i.agregar_condicion(5,"buzz")

        # jojo = i.agregar_condicion(10,"jojo")


        simultaños = [fizz,buzz]
        i.agregar_simultaño(simultaños)

        texto = i.devolver_lista_a_imprimir()

        cant  = 1
        for each in texto:
            print (f"--{cant}-{each} ")
            cant += 1

if __name__ == '__main__':
    if len(sys.argv) == 3 :
        primer_enunciado((sys.argv[1],sys.argv[2]))
    else:
        print ("Ingrese el rango de numero  'Inicio'  'Fin' ")