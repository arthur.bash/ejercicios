from Diagnostico import Imprimidor
import sys

def segundo_enunciado(num):
    i = Imprimidor()
    fizz = i.agregar_condicion(3,"fizz")
    buzz = i.agregar_condicion(5,"buzz")

    
    print(i.fizzBuzz(num))

if __name__ == '__main__':
    if len(sys.argv) == 2:
        segundo_enunciado(sys.argv[1])
    else:
        print("Ingrese un numero para obtener el mensaje correpondiente")

