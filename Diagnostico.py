
"""Se agrego el metodo corresponde, en msj_multiplo y fizzBuzz"""
class Msj_multiplo():
    msj = ""
    
    def __init__(self,multiplo,msj =  None):
        self.msj = msj
        self.multiplo = multiplo
        self.simultaño = False

    def sos_multiplo(self,numero):
        return numero % self.multiplo == 0

    def perteneces_a_simultaño(self):
        self.simultaño = True   

    def sos_simultaño(self):
        return self.simultaño 

    def corresponde(self,multiplo):
        return self.multiplo == multiplo

    
    def __str__(self):
        return self.msj

class Simultaño():
    lista_objs_multiplo = []

    def __init__(self,lista_objs_multiplo):
        for obj_multiplo_simultaño in lista_objs_multiplo:
            obj_multiplo_simultaño.perteneces_a_simultaño()
        self.lista_objs_multiplo = lista_objs_multiplo
    
    
    def cantidad_objs_multiplo(self):
        return len(self.lista_objs_multiplo)

    def __str__(self):
        return self.msj()

    def msj(self):
        msj = ""
        for multiplo in self.lista_objs_multiplo:
            msj += str(multiplo).capitalize()
        return msj 

class Imprimidor():
    cantidad_objs_multiplo_a_devolver = 0
    lista_a_devolver = []
    lista_a_comparar = []
    

    def __init__(self,inicio = 0,fin = 0):
        self.inicio = inicio
        self.fin = fin
        

    def agregar_condicion(self,multiplo,msj):
        obj_multiplo = Msj_multiplo(multiplo,msj)
        self.lista_a_comparar.append(obj_multiplo)
        return obj_multiplo

    def agregar_simultaño(self,lista_objs_multiplo):
        self.simultaño = Simultaño(lista_objs_multiplo)

    def fizzBuzz(self,numero):
        for multiplo in self.lista_a_comparar:
            if multiplo.corresponde(numero):
                return multiplo
        return numero

    

    def devolver_lista_a_imprimir(self):
        self.comparar_multiplos()
        return self.lista_a_devolver

    def decidir_msj(self,cant_numeros_comparables):
        msj_multiplo = []
        cant_simultaño = 0
        for compara in self.lista_a_comparar:
            if compara.sos_multiplo(cant_numeros_comparables):
                if compara.sos_simultaño():
                    cant_simultaño += 1
                msj_multiplo.append(compara)
                
        

        if cant_simultaño >= self.simultaño.cantidad_objs_multiplo():
            return self.simultaño
        
        msj_a_devolver = ""
        if msj_multiplo:
            for obj_msj in msj_multiplo:
                msj_a_devolver += str(obj_msj) + " , "
        else:
            msj_a_devolver = str(cant_numeros_comparables)    

        return msj_a_devolver
            
        
    def comparar_multiplos(self):
        #Compara todos los numeros del rango, con la lista de multiplos con mensajes
        cant_numeros_comparables = self.inicio
        while cant_numeros_comparables <= self.fin :
            msj = self.decidir_msj(cant_numeros_comparables)    
            self.lista_a_devolver.append(msj)
            cant_numeros_comparables += 1








